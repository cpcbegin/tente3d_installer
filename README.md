# Multi-construction LDraw configuration kit
![Tente 3D](https://raw.githubusercontent.com/cpcbegin/tente3d_installer/master/images/bannertente.png)
## Content
This script installs and configured these applications to create and see 3D models of several construction systems:

* MLCad 3.40
* BMP2LDraw
* LDDesignPad
* LD4DStudio
* ldglite_1.3.1
* ldraw_2017.01
* ldview_4.3-ubuntu18.04
* leocad_17.07
* lpub_4.0
* povray 3.7
* Blender

## Other resources:

* LDraw TENTE set (Jasolo)
* LDraw LEGO set (http://www.ldraw.org/)
* LDraw EXIN CASTILLOS set: COMMING SOON!
* Mis construcciones 3D, tanto customs como reproducciones de oficiales.

## Ubuntu Linux installation

1. Download this kit in any folder.
1. Open console and select this folder.
1. Type:
sudo ./install.sh

## Windows installation
For now you need to install this set manually in Windows:

1. Download [LDView](http://ldview.sourceforge.net) and install it.
1. Make the folder C:\LDRAW and uncompress this kit in this folder.
1. Uncompress these set of pieces in these folders:
   * [C:\LDRAW\TENTE](https://www.dropbox.com/s/irba95qphdxtiq7/LDrawTente_Ultima.zip?dl=0).
   * [C:\LDRAW\EXINCASTILLOS](http://www.exincastillos.es/foro/creando-piezas-virtuales.html) (you need authenticate in this web).
   * [C:\LDRAW\LEGO](http://www.ldraw.org).
1. Uncompress these software in these folders:
   * [MLCad 3.40](http://mlcad.lm-software.com/e_default.htm) in C:\LDRAW\SOFTWARE\MLCAD_V3.40 (Optional).
   * [BMP2LDRAW](http://205.196.123.184/t75cw79px9zg/tvswby3h5qs/BMP2LDraw.zip) in C:\LDRAW\SOFTWARE\BMP2LDRAW
   * [LDDesignPad](http://lddp.sourceforge.net/) in C:\LDRAW\SOFTWARE\LDDP
   * Etc...
1. Copy in Desktop the files of this folder C:\ldraw\software\accesos-directos-Windows.

Script created by [Jesus B.L. "CPCBEGIN"](http://malagaoriginal.blogspot.com.es/search/label/3D) under GNU/GPL 3.0 license.




